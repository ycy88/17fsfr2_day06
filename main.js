const express = require('express');

// new instance of express 
var app = express();

// routes
app.get('/', function(req, res){
  res.status(200);
  res.type('text/plain');
  res.end('This is the ROOT route');
});

app.use(express.static(__dirname + '/public'));

// configure ports 
var port = process.argv[2] || 3000;

app.listen(port, function(){
  console.log('Now listening on port ' + port);
});
